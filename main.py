import boto3
import os
import numpy as np
import sklearn
import time
import pickle
import configparser
config = configparser.ConfigParser()
config.read('aws_s3.config', encoding='ascii')

ACCESS_KEY = config.get('DEFAULT', 'ACCESS_KEY')
SECRET_KEY = config.get('DEFAULT', 'SECRET_KEY')

INPUT_BUCKET = 'zoomi-accenture-heatmap-inputs'



def get_course_texts():
    # s3 = boto3.resource('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
    # bucket = s3.Bucket(INPUT_BUCKET)
    # course_folders = list(set([obj.key.split('/')[0] for obj in bucket.objects.filter()]))
    # course_data = {}
    # for course in course_folders:
    #     print course
    #     course_data[course] = retrieve_docs(course)
    #     print '\n'
    # pickle.dump(course_data, open("course_data.pickle", "wb"))
    return



def retrieve_docs(course_name):
    try:
        s3 = boto3.resource('s3', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
        bucket = s3.Bucket(INPUT_BUCKET)
        print bucket.objects
        documents = dict()
        print bucket.objects.filter(Prefix=course_name)
        num_docs = 0
        for obj in bucket.objects.filter(Prefix=course_name + '/'):
            key = obj.key
            if key == course_name + '/': continue
            # print "key",key, "obj", obj
            body = obj.get()['Body'].read()
            # index = int(key.split('/')[1].split()[0])
            documents[num_docs] = body
            num_docs += 1
    except Exception as e:
        raise e
        return "Unable to read documents from s3."
    print '-------- finish retrieving doc -------------'
    return documents



def check(bag):
    flags = ['click', 'select', 'button', 'next', 'slide', 'previous', 'congratulation', 'congratulations',
             'proceed', 'return', 'menu']
    for fl in flags:
        if fl in bag:
            return True
    return False

def process_words(bag):
    return [w.lower() for w in bag]





def manual_labelling():

    course_data = pickle.load(open("course_data.pickle", "rb"))
    labels = {}

    for course in course_data:
        if 'reg' in course:
            continue

        labels[course] = {}
        for seg_ind in course_data[course]:
            print course_data[course][seg_ind]
            label = raw_input("\n************** \n%s %s \nIs this slide instructional? Enter y/n: " %(course, seg_ind))
            while label not in ['y', 'n', 'quit']:
                label = raw_input("%s %s \nIs this slide instructional? Enter y/n/quit: " % (course, seg_ind))

            if label == 'quit':
                break

            labels[course][seg_ind] = label
            print('------------------------------------------\n\n\n')

        if label == 'quit':
            break

    pickle.dump(labels, open("course_data_labels_%s.pickle" %(str(int(time.time()))), "wb"))

    return 0



if __name__ == '__main__':

    course_data = pickle.load(open("course_data.pickle", "rb"))
    course_data_labels = pickle.load(open("course_data_labels_1546464734.pickle", "rb"))
    corrections = [  ['PDG_HCP', 15],
                     ['PDG_PTP_BIO2629', 7],
                     ['PDG_PTP_BIO2629', 78],
                     ['CMP_Respect', 27],
                     ['CITI_Phishing_Defense',  57],
                     ['CITI_Phishing_Defense',  58],
                     ['CITI_conduct_risk_training_2019', 0],
                     ['CITI_conduct_risk_training_2019', 1],
                     ['CITI_conduct_risk_training_2019', 23],
                     ['ITC_seg', 1],
                     ['Quality_Leadership_Training', 21],
                     ['HAZCOM_SL3', 98],
                     ['PDG_OLP', 38]
                     ]

    for cor in corrections:
        c, ind = cor[0], cor[1]
        course_data_labels[c][ind] = 'y'

    pickle.dump(course_data_labels, open("course_data_labels_%s.pickle" % (str(int(time.time()))), "wb"))



